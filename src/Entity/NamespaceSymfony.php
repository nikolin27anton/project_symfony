<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class NamespaceSymfony
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=InterfaceSymfony::class, mappedBy="namespaceSymfony")
     */
    private $interfaceSymfony;

    /**
     * @ORM\OneToMany(targetEntity=ClassSymfony::class, mappedBy="namespaceSymfony")
     */
    private $classesSymfony;

    /**
     * @ORM\ManyToOne(targetEntity=NamespaceSymfony::class, inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=NamespaceSymfony::class, mappedBy="parent")
     */
    private $children;

    /**
     * NamespaceSymfony constructor.
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->interfaceSymfony = new ArrayCollection();
        $this->classesSymfony = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return Collection|InterfaceSymfony[]
     */
    public function getInterfaceSymfony(): Collection
    {
        return $this->interfaceSymfony;
    }

    public function addInterfaceSymfony(InterfaceSymfony $interfaceSymfony): self
    {
        if (!$this->interfaceSymfony->contains($interfaceSymfony)) {
            $this->interfaceSymfony[] = $interfaceSymfony;
            $interfaceSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeInterfaceSymfony(InterfaceSymfony $interfaceSymfony): self
    {
        if ($this->interfaceSymfony->removeElement($interfaceSymfony)) {
            // set the owning side to null (unless already changed)
            if ($interfaceSymfony->getNamespaceSymfony() === $this) {
                $interfaceSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClassSymfony[]
     */
    public function getClassesSymfony(): Collection
    {
        return $this->classesSymfony;
    }

    public function addClassesSymfony(ClassSymfony $classesSymfony): self
    {
        if (!$this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony[] = $classesSymfony;
            $classesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeClassesSymfony(ClassSymfony $classesSymfony): self
    {
        if ($this->classesSymfony->removeElement($classesSymfony)) {
            // set the owning side to null (unless already changed)
            if ($classesSymfony->getNamespaceSymfony() === $this) {
                $classesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }
}
