<?php

namespace App\Controller;

use Mpdf\Mpdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class MpdfGeneratorController extends AbstractController
{
    /**
     * @Route ("/mpdf", name="pdf_site")
     */
    public function pdfGeneration()
    {
        $html = file_get_contents('https://www.google.com');
        $mpdf = new Mpdf();
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($html);
        $mpdf->Output(__DIR__.'/../../public/pdf/google_site.pdf', 'F');

        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/google_site.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'google_site.pdf');

        return $response;
    }
}
