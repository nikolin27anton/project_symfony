<?php

namespace App\Controller;

use App\Entity\User;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class ExportController extends AbstractController
{
    /**
     * @Route ("/pdf", name="pdf_google_site")
     */
    public function pdfGeneration(Pdf $generator)
    {
        $generator->generate('http://www.google.com', __DIR__.'/../../public/pdf/google_site.pdf');

        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/google_site.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'google_site.pdf');

        return $response;
    }

    /**
     * @Route ("/xls", name="xls_hello_world")
     */
    public function xlsHelloWorld()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Anton');
        $sheet->setCellValue('A2', 'Nikolin');

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__.'/../../public/xls/hello_world.xlsx');

        $response = new BinaryFileResponse(__DIR__.'/../../public/xls/hello_world.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.xlsx');

        return $response;
    }

    /**
     * @Route ("/download/user/xls", name="download_user_xls")
     */
    public function downloadUser()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();

        $excel = new Spreadsheet();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        $sheet->setTitle('Пользователи');

        $row = 3;
        $style = [
            'borders' => [
                'allborders' => [
                    'style' => Border::BORDER_DOUBLE,
                ],
            ],
        ];

        foreach ($users as $user) {
            $sheet
                ->setCellValue('B2', 'Имя')
                ->setCellValue('C2', 'ID')
                ->setCellValue('B'.$row, $user->getUsername())
                ->setCellValue('C'.$row, $user->getId());
            ++$row;
        }
        --$row;
        $sheet->getStyle('B2:P'.$row)->applyFromArray($style);
        $objWriter = new Xls($excel);
        $filename = __DIR__.'/../../public/xls/users.xls';
        $objWriter->save($filename);

        return new BinaryFileResponse($filename);
    }

    /**
     * @Route ("/qr", name="generate_qr")
     */
    public function generateQrCode()
    {
        $writer = new PngWriter();

        $qrCode = QrCode::create('https://football.ua/')
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

//        $logo = Logo::create(__DIR__.'/../../public/qr/logo.png')
//            ->setResizeToWidth(50);

//        $label = Label::create('Label')
//            ->setTextColor(new Color(255,0,0));

        $result = $writer->write($qrCode);

        $result->saveToFile(__DIR__.'/../../public/qr/qr_code_new.png');

        $response = new BinaryFileResponse(__DIR__.'/../../public/qr/qr_code_new.png');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'qrcode.png');

        return $response;
    }
}
