<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    /**
     * @Route("/email")
     */
    public function sendEmail(MailerInterface $mailer): Response
    {
        $email = (new Email())
            ->from('nikolinanton27@gmail.com')
            ->to('nikolinanton27@gmail.com')
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again')
            ->html('<h1>See Twig integration for better HTML integration</h1>>');

        $mailer->send($email);

        return $this->render('default/send_email.html.twig');
    }
}
