<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArticleController extends AbstractController
{
//    public function indexAction()
//    {
//        $em = $this->getDoctrine()->getManager();
//        $articles = $em->getRepository(Article::class)->findAll();
//
//        return $this->render()
//    }

//    /**
//     * @Route("/article", name="article_create")
//     */

//    public function createArticle(ValidatorInterface $validator): Response
//    {
//        // you can fetch the EntityManager via $this->getDoctrine()
//        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
//      //$timestamp = date('Y-m-d H:i:s', time());
//        $timestamp = new \DateTime();
//
//
//
//        $entityManager = $this->getDoctrine()->getManager();
//
//        $article = new Article();
//        $article->setName('Четверг');
//        $article->setDescription('рабочий');
//        $article->setCreatedAt($timestamp);
//
//        // tell Doctrine you want to (eventually) save the Product (no queries yet)
//        $entityManager->persist($article);
//
//
//        // Валидация, проверка
    ////        $errors = $validator->validate($article);
    ////        if(count($errors) >0)
    ////        {
    ////            return new Response((string) $errors,400);
    ////        }
//
//        // actually executes the queries (i.e. the INSERT query)
//        $entityManager->flush();
//
//        return  new Response('Saved new article with id '.$article->getId());
//
//
//
//    }

//    public function show(int $id, ArticleRepository $articleRepository):Response
//    {
//        //$article = $this->getDoctrine()->getRepository(Article::class)->find(id);
//        $article= $articleRepository->find(22);
//        return new Response('article'.$article->getName());
//    }

    /**
     * @Route ("/article/{_locale}", name = "article_index", requirements={"_locale"="en|ru"})
     */
    public function indexAction(Request $request, PaginatorInterface $paginator)
    {
//        $em = $this->getDoctrine()->getManager();
//        $article = $em->getRepository(Article::class)->findAll();
//
//        return $this->render('article/index.html.twig', ['articles' => $article]);

        $query = $this->getDoctrine()
                ->getRepository(Article::class)
                ->createQueryBuilder('c');

        $article = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->getSession()->get('items', $request->query->get('items', 10))
        );

        return $this->render('article/index.html.twig', ['articles' => $article]);
    }

    /**
     * @Route ("/article/{_locale}/create", name = "article_create", requirements={"_locale"="en|ru"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/create.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route ("/article/{_locale}/edit/{id}", name="article_edit", requirements={"_locale"="en|ru"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }
        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/edit.html.twig', ['article' => $article, 'editForm' => $editForm->createView()]);
    }

    /**
     * @Route ("article/{_locale}/delete/{id}", name = "article_delete", requirements={"_locale"="en|ru"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID '.$id.'not found!');
        }

        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('article_index');
    }

    /**
     * @Route("/article/locale/{locale}", name="article_locale")
     */
    public function changeLocaleAction(Request $request, string $locale)
    {
        $request->getSession()->set('_locale', $locale);

        return $this->redirectToRoute('article_index', ['_locale' => $locale]);
    }
}
