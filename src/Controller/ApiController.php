<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("package/", name="package_all")
     */
    public function indexAction()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/packages/list.json');
        $packages = json_decode($response->getContent(), true);

        return $this->render('default/index.html.twig', ['packages' => $packages['packageNames']]);
    }

    /**
     * @Route("package/search/{name}", name = "package_search")
     */
    public function searchAction($name)
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/search.json?q='.$name);
        $packages = json_decode($response->getContent(), true);

        return $this->render('default/search.html.twig', ['packages' => $packages['results']]);
    }

    /**
     * @Route("package/search/vendor/{vendor}", name="package_vendor")
     */
    public function searchVendorAction($vendor)
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/packages/list.json?vendor='.$vendor);
        $packages = json_decode($response->getContent(), true);

        return $this->render('default/index.html.twig', ['packages' => $packages['packageNames']]);
    }
}
