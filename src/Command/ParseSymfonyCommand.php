<?php

namespace App\Command;

use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ParseSymfonyCommand constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
           ->setName('app:parse-symfony')
           ->setDescription('Parsing site api.andreybolonin.com')
           ->setHelp('This command parses the site');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $html = file_get_contents('http://api.andreybolonin.com');

        $crawler = new Crawler($html);
        //$filtered = $crawler->filter('div.page-content > div.namespace.clearfix > div.namespace-container > ul > li > a');
        $filtered = $crawler->filter('div.namespace-container > ul > li >a');

        // var_dump($filtered->textContent());
        foreach ($filtered as $value) {
            $url = 'http://api.andreybolonin.com/'.str_replace('../', '', $value->getAttribute('href'));
            $set = new NamespaceSymfony();
            $set->setName($value->textContent);
            $set->setUrl($url);
            $this->entityManager->persist($set);
        }
        $this->entityManager->flush();

        return 1;
    }
}
