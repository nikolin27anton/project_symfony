<?php

namespace App\Repository;

use App\Entity\InterfaceSymfony;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InterfaceSymfony|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterfaceSymfony|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterfaceSymfony[]    findAll()
 * @method InterfaceSymfony[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterfaceSymfonyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterfaceSymfony::class);
    }

    // /**
    //  * @return InterfaceSymfony[] Returns an array of InterfaceSymfony objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InterfaceSymfony
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
