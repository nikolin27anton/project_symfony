<?php

namespace App\Repository;

use App\Entity\ClassSymfony;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClassSymfony|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassSymfony|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassSymfony[]    findAll()
 * @method ClassSymfony[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassSymfonyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassSymfony::class);
    }

    // /**
    //  * @return ClassSymfony[] Returns an array of ClassSymfony objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClassSymfony
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
