<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class XlsHtmlPdfTest extends WebTestCase
{
    public function testPdf()
    {
        $client = static :: createClient();
        $client->request('GET', '/mpdf');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/pdf');
    }

    public function testXls()
    {
        $client = static :: createClient();
        $client->request('GET', '/xls');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

    public function testQr()
    {
        $client = static::createClient();
        $client->request('GET', '/qr');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'image/png');
    }
}
