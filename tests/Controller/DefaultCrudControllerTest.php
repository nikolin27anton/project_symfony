<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest.
 */
class DefaultCrudControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register/ru');
        $button = $crawler->selectButton('user_create[submit]');
        $form = $button->form([
            'user_create[username]' => 'user',
            'user_create[password][first]' => 'test123',
            'user_create[password][second]' => 'test123',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/article/login/ru');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testIndexAction()
    {
        $client = static :: createClient();
        $crawler = $client->request('GET', '/article/login/ru');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/ru'); //composer req symfony/browser-kit
        $this->assertEquals(200, $client->getResponse()->getStatusCode()); //composer require phpunit/phpunit
    }

    public function testCreateAction()
    {
        $client = static :: createClient();
        $crawler = $client->request('GET', '/article/login/ru');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/ru/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'create_test',
            'article[description]' => 'yes_created',
            'article[created_at][date][day]' => '30',
            'article[created_at][date][month]' => '4',
            'article[created_at][date][year]' => '2021',
            'article[created_at][time][hour]' => '4',
            'article[created_at][time][minute]' => '19',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditAction()
    {
        $client = static :: createClient();
        $crawler = $client->request('GET', '/article/login/ru');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/ru/edit/1');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test_edit',
            'article[description]' => 'yes_edits',
            'article[created_at][date][day]' => '27',
            'article[created_at][date][month]' => '11',
            'article[created_at][date][year]' => '2022',
            'article[created_at][time][hour]' => '0',
            'article[created_at][time][minute]' => '40',
        ]);

        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteAction()
    {
        $client = static :: createClient();
        $crawler = $client->request('GET', '/article/login/ru');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/ru/delete/1');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testSendEmailAction()
    {
        $client = static :: createClient();
        $client->request('GET', '/email');

        $this->assertEmailCount(1);
    }
}
